(function ($, Drupal) {
  'use strict';

  /**
   * Checks support for a client-side data storage bins.
   *
   * @param {String} bin
   *   The space to store in, one of 'session', 'local', 'global'.
   */
  var supported = function (bin) {
    try { return bin + 'Storage' in window && window[bin + 'Storage'] !== null; }
    catch (e) { return false; }
  };

  /**
   * Temporary memory storage.
   *
   * This is only used as a temporary stop gap measure to prevent code that
   * relies on a valid Storage API being available.
   */
  var tempStorage = {
    _: {},
    setItem: function (id, val) {return this._[id] = String(val);},
    getItem: function (id) {return this._.hasOwnProperty(id) ? this._[id] : void 0;},
    removeItem: function (id) {return delete this._[id];},
    clear: function () {return this._ = {};}
  };

  /**
   * Drupal HTML5 storage handler.
   *
   * @param {String} prefix
   *   A prefix to use for all keys.
   *
   * @see https://www.drupal.org/node/65578
   *
   * @constructor
   */
  var Storage = function (prefix) {
    this.tempStorage = tempStorage;
    this.prefix = prefix || 'Drupal';
    this.support = {
      local: supported('local'),
      session: supported('session')
    };
  };

  /**
   * Retrieves the actual bin object based on the string passed.
   *
   * @param {String} bin
   *   The bin name to load, can be one of "local" or "session".
   *
   * @return {localStorage|sessionStorage|tempStorage}
   *   A proper storage object, tempStorage if it cannot find one.
   */
  Storage.prototype.getBin = function (bin) {
    bin = bin || 'local';
    bin = bin.toLowerCase().replace(/storage$/, '');
    return this.support[bin] ? window[bin + 'Storage'] : this.tempStorage;
  };

  /**
   * Retrieves the prefixed key.
   *
   * @param {String} key
   *   The key, un-prefixed.
   *
   * @return {String}
   */
  Storage.prototype.getKey = function (key) {
    // Escape any characters in the prefix.
    var prefix = (this.prefix + '').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
    // Remove any prefix from the key and turn it into an array.
    var keys = key.replace(new RegExp('^' + prefix + '\\.?'), '').split('.');
    // Prepend the prefix.
    keys.unshift(this.prefix);
    // Join them back together.
    return keys.join('.');
  };

  /**
   * Loads data from client-side storage.
   *
   * @param {String} key
   *   The key name to load stored data from. Automatically prefixed with
   *   "Drupal.".
   * @param {String} [bin]
   *   (Optional) A string denoting the storage space to read from. Defaults to
   *   'local'. See Enhancements.storage.save() for details.
   *
   * @return {*}
   *   The data stored or null.
   *
   * @see Storage.save
   */
  Storage.prototype.load = function (key, bin) {
    key = this.getKey(key);
    var storage = this.getBin(bin);
    var item = storage ? storage.getItem(key) : void 0;
    return item !== void 0 ? window.JSON.parse(item) : null;
  };

  /**
   * Stores data on the client-side.
   *
   * @param key
   *   The key name to store data under. Automatically prefixed with "Drupal.".
   *   Should be further namespaced by module; e.g., for
   *   "Drupal.module.settingName" you pass "module.settingName".
   * @param data
   *   The data to store.
   * @param bin
   *   (optional) A string denoting the storage space to store data in:
   *   - session: Reads from window.sessionStorage. Persists for currently opened
   *     browser window/tab only.
   *   - local: Reads from window.localStorage. Stored values are only available
   *     within the scope of the current host name only.
   *   - global: Reads from window.globalStorage.
   *   Defaults to 'local'.
   *
   * @return {Boolean}
   *   Indicates saving succeeded or not.
   * @see Storage.load
   */
  Storage.prototype.save = function (key, data, bin) {
    key = this.getKey(key);
    var storage = this.getBin(bin);
    return storage && storage.setItem(key, window.JSON.stringify(data)) ? true : false;
  };

  /**
   * Delete data from client-side storage.
   *
   * Called 'remove', since 'delete' is a reserved keyword.
   *
   * @param key
   *   The key name to delete. Automatically prefixed with "Drupal.".
   * @param bin
   *   (optional) The storage space name. Defaults to 'session'.
   *
   * @see Storage.save
   */
  Storage.prototype.remove = function (key, bin) {
    key = this.getKey(key);
    var storage = this.getBin(bin);
    return storage && storage.removeItem(key);
  };

  /**
   * Parses a stored value into its original data type.
   *
   * HTML5 storage always stores values as strings. This is a "best effort" to
   * restore data type sanity.
   */
  Storage.prototype.parse = function (val) {
    // Convert booleans.
    if (val === 'true') {
      val = true;
    }
    else if (val === 'false') {
      val = false;
    }
    // Convert numbers.
    else if (/^[0-9.]+$/.test(val)) {
      val = parseFloat(val);
    }
    return val;
  };

  /**
   * Serializes a value suitable for client-side (string) storage.
   */
  Storage.prototype.serialize = function (val) {
    return $.param(val);
  };

  /**
   * Unserializes a $.param() string.
   *
   * Note that this only supports simple values (numbers, booleans, strings)
   * and only an one-dimensional (flat) associative configuration object (due to
   * limitations of jQuery.param()).
   */
  Storage.prototype.unserialize = function (str) {
    var obj = {};
    jQuery.each(str.split('&'), function() {
      var splitted = this.split('=');
      if (splitted.length !== 2) {
        return;
      }
      var key = decodeURIComponent(splitted[0]);
      var val = decodeURIComponent(splitted[1].replace(/\+/g, ' '));
      val = this.parse(val);

      // Ignore empty values.
      if (typeof val === 'number' || typeof val === 'boolean' || val.length > 0) {
        obj[key] = val;
      }
    });
    return obj;
  };

  Drupal.Storage = Drupal.Storage || Storage;

})(jQuery, Drupal);
