/**
 * @file
 * User Enhancements handler.
 */

(function ($, Drupal) {
  'use strict';

  /**
   * Creates a new UserEnhancements handler object.
   *
   * @namespace UserEnhancements
   */
  window.UserEnhancements = {

    /**
     * Flag indicating whether or not code is running in "debug" mode.
     *
     * @type {Boolean}
     */
    debug: Drupal.settings && Drupal.settings.userEnhancements && !!Drupal.settings.userEnhancements.debug || true,

    /**
     * A list of UserEnhancement objects, keyed by their machine name.
     *
     * @type {Object.<string, UserEnhancement>}
     */
    enhancements: {},

    /**
     * The settings object from Drupal.settings.
     *
     * @type {Object}
     */
    settings: Drupal.settings && Drupal.settings.userEnhancements || {},

    /**
     * Behavior attachment handler for user enhancements.
     *
     * @see Drupal.attachBehaviors
     */
    attach: function (context, settings) {
      /** @type {UserEnhancement} */
      var enhancement;
      for (var i in this.enhancements) {
        if (this.enhancements.hasOwnProperty(i) && (enhancement = this.enhancements[i])) {
          for (var s in enhancement.attachments) {
            if (enhancement.attachments.hasOwnProperty(s)) {
              enhancement.attachments[s].apply(enhancement, arguments)
            }
          }
        }
      }
    },

    /**
     * Creates a user enhancement.
     *
     * @param {String} name
     *   The machine name of the user enhancement to create. If an existing
     *   namespaced (using dot notation) user enhancement exists, it will be
     *   used as the "parent" object that this new object is created from. This
     *   effectively allows user enhancements to automatically inherit their
     *   parent's object, e.g. a user enhancement with the machine name:
     *   "parent.child.grandchild" would automatically inherit properties and
     *   methods from the "parent.child" and "parent" user enhancement objects,
     *   if they existed.
     * @param {...Object} [obj]
     *   Additional objects used to create the initial properties and functions
     *   of the new user enhancement object.
     *
     * @return {UserEnhancement}
     *   A user enhancement instance.
     */
    create: function (name, obj) {
      var args = Array.prototype.slice.call(arguments);
      name = args.shift();

      if (typeof name !== 'string') {
        this.fatal('The first argument in UserEnhancements.create() must be a string indicating the machine name of the user enhancement to create.');
      }

      if (this.enhancements[name]) {
        this.warning('An instance for the "' + name + '" user enhancement already exists and cannot be recreated, returning existing instance. Use the UserEnhancements.get() method to retrieve or use the UserEnhancements.extend() methods to extend an existing instance.')
        return this.enhancements[name];
      }

      // Find any namespaced parent objects.
      var namespaces = name.split('.');
      var enhancement;
      var parent;
      while (namespaces.length) {
        parent = this.get(namespaces.join('.'), true);
        if (parent) {
          break;
        }
        namespaces.pop();
      }

      /**
       * @type {UserEnhancement}
       */
      enhancement = Object.create(parent ? parent : window.UserEnhancement);

      // Ensure the element has explicit empty default property values.
      enhancement.$element = $();
      enhancement.$wrapper = $();
      enhancement.attachments = {};
      enhancement.deferred = {};
      enhancement.detachments = {};
      enhancement.init = function () {};
      enhancement.initialized = false;
      enhancement.settings = {};
      enhancement.storage = new Drupal.Storage('UserEnhancement.' + name);

      // Merge in any passed argument objects.
      enhancement.extend.apply(enhancement, args);

      // Ensure/override the following properties after any of the above
      // objects are merged in.
      enhancement.name = name;
      enhancement.__parent__ = parent;
      enhancement.debug = this.debug;
      enhancement.setSetting(this.settings[name]);

      // Initialize the instance.
      enhancement.init();
      enhancement.initialized = true;

      this.enhancements[name] = enhancement;

      return enhancement;
    },

    /**
     * Behavior detachment handler for user enhancements.
     *
     * @see Drupal.detachBehaviors
     */
    detach: function () {
      /** @type {UserEnhancement} */
      var enhancement;
      for (var i in this.enhancements) {
        if (this.enhancements.hasOwnProperty(i) && (enhancement = this.enhancements[i])) {
          for (var s in enhancement.detachments) {
            if (enhancement.detachments.hasOwnProperty(s)) {
              enhancement.detachments[s].apply(enhancement, arguments)
            }
          }
        }
      }
    },

    /**
     * Displays an error message, if debugging is enabled.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    error: function (message) {
      this.debug && window.console && window.console.error && window.console.error.apply(window.console, arguments);
    },

    /**
     * Throws a fatal error message.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    fatal: function (message) {
      throw new Error(message);
    },

    /**
     * Retrieves a user enhancement instance.
     *
     * @param {String} name
     *   The machine name of the user enhancement.
     * @param {Boolean} [suppressError]
     *   Toggle determining whether or not to suppress any errors if the user
     *   enhancement does not exist.
     *
     * @returns {UserEnhancements.enhancements.}
     */
    get: function (name, suppressError) {
      var enhancement = this.enhancements[name] || null;
      if (!enhancement && !suppressError) {
        this.error('The user enhancement "' + name + '" does not exist. You must first create the user enhancement before you can retrieve it.');
      }
      return enhancement;
    },

    /**
     * Displays an informative message, if debugging is enabled.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    info: function (message) {
      this.debug && window.console && window.console.info && window.console.info.apply(window.console, arguments);
    },

    /**
     * Displays a warning message, if debugging is enabled.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    warning: function (message) {
      this.debug && window.console && window.console.warn && window.console.warn.apply(window.console, arguments);
    }

  };

  // Tie the UserEnhancements handler object into Drupal's behavior system.
  Drupal.behaviors.userEnhancements = window.UserEnhancements;

})(jQuery, Drupal);
