/**
 * @file
 * User Enhancements core functionality.
 */

(function ($, Drupal) {
  'use strict';

  /**
   * Creates a new UserEnhancement object.
   *
   * @namespace UserEnhancement
   *
   * @extends {Object}
   */
  window.UserEnhancement = {

    /**
     * A parent user enhancement object, if any.
     *
     * @type {UserEnhancement}
     */
    __parent__: null,

    /**
     * The element for this user enhancement object.
     *
     * @type {jQuery}
     */
    $element: $(),

    /**
     * The wrapper around the element for this user enhancement object.
     *
     * @type {jQuery}
     */
    $wrapper: $(),

    /**
     * The attachments object for this user enhancement object.
     *
     * @type {Object}
     */
    attachments: {},

    /**
     * Flag indicating whether or not code is running in "debug" mode.
     *
     * @type {Boolean}
     */
    debug: false,

    /**
     * Stores any deferred events.
     *
     * @type {Object}
     */
    deferred: {},

    /**
     * The detachments object for this user enhancement object.
     *
     * @type {Object}
     */
    detachments: {},

    /**
     * A callback to invoke when initializing the object.
     *
     * @type {Function}
     */
    initCallback: function () {
    },

    /**
     * Flag indicating whether the object has been fully initialized.
     *
     * @type {boolean}
     */
    initialized: false,

    /**
     * The machine name of the user enhancement.
     *
     * @type {String}
     */
    name: name,

    /**
     * The settings object for this user enhancement object.
     *
     * @type {Object}
     */
    settings: {},

    /**
     * The Storage object for this user enhancement object.
     *
     * @type {Drupal.Storage}
     */
    storage: new Drupal.Storage('UserEnhancement.' + name),

    /**
     * Creates an attachment behavior for the user enhancement.
     *
     * @param {string} selectors
     *   The selector(s) on which to bind this behavior.
     * @param {Function} [callback]
     *   Optional. A callback function to implement when the behavior is
     *   attached to the element as defined by the selector.
     *
     * @see Drupal.attachBehaviors
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    attach: function (selectors, callback) {
      var self = this;
      this.attachments[selectors] = function (context, settings) {
        // Only attach behaviors that are not already attached (similar to $.once).
        var $selectors = $(context).find(selectors).filter(function () {
          return !$(this).data('Drupal.userEnhancement.' + self.name);
        });
        if ($selectors[0]) {
          self.$selectors = $selectors;
          self.__args__ = arguments;
          self.$selectors.data('Drupal.userEnhancement.' + self.name, self);
          callback.apply(self, [self.$selectors, self.settings]);
          delete self.__args__;
        }
      };
      return this;
    },

    /**
     * Attaches defined elements to the DOM and creates an attachment behavior.
     *
     * @param {String} method
     *   The method on which to invoke on the selectors to attach the elements.
     * @param {string} selectors
     *   The selector(s) on which to bind this behavior.
     * @param {Function} [callback]
     *   Optional. A callback function to implement when the behavior is
     *   attached to the element as defined by the selector.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    attachElements: function (method, selectors, callback) {
      this.attach(selectors, function ($selectors) {
        var parts = (method + ':*').split(':');
        var filter = parts[1] === '*' ? parts[1] : ':' + parts[1];
        method = parts[0];
        if (this.$wrapper[0]) {
          this.$wrapper.append(this.$element);
          $selectors.filter(filter)[method](this.$wrapper);
        }
        else {
          $selectors.filter(filter)[method](this.$element);
        }
        if (callback) {
          callback.apply(this, arguments);
        }
      }.bind(this));
      return this;
    },

    /**
     * Creates an detachment behavior for the user enhancement.
     *
     * @param {string} selectors
     *   The selector(s) on which to bind this behavior.
     * @param {Function} [callback]
     *   Optional. A callback function to implement when the behavior is
     *   attached to the element as defined by the selector.
     *
     * @see Drupal.detachBehaviors
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    detach: function (selectors, callback) {
      var self = this;
      this.detachments[selectors] = function (context, settings, trigger) {
        var $selectors = $(context).find(selectors);
        if ($selectors[0]) {
          self.__args__ = arguments;
          self.$selectors = $selectors;
          callback.apply(self, [self.$selectors, self.settings]);
          self.$selectors.removeData('Drupal.userEnhancement.' + self.name);
          delete self.__args__;
        }
      };
      return this;
    },

    /**
     * Detaches defined elements from the DOM and creates a detachment behavior.
     *
     * @param {string} selectors
     *   The selector(s) on which to bind this behavior.
     * @param {Function} [callback]
     *   Optional. A callback function to implement when the behavior is
     *   attached to the element as defined by the selector.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    detachElements: function (selectors, callback) {
      this.detach(selectors, function () {
        this.$element.remove();
        this.$wrapper.remove();
        if (callback) {
          callback.apply(this, arguments);
        }
      }.bind(this));
      return this;
    },

    /**
     * Displays an error message, if debugging is enabled.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    error: function (message) {
      if (this.debug && window.console && window.console.error) {
        window.console.error.apply(window.console, arguments);
      }
    },

    /**
     * Extends a user enhancement.
     *
     * @param {Boolean} [deep]
     *   Flag determining whether or not all inherited properties should be
     *   extended from any passed objects.
     * @param {...Object} objects
     *   The objects to extend from.
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    extend: function (deep, objects) {
      var args = Array.prototype.slice.call(arguments);
      deep = false;
      if (args[0] === true || args[0] === false) {
        deep = args.shift();
      }
      args.forEach(function (obj) {
        Object.keys(obj).forEach(function (key) {
          if (!deep && !obj.hasOwnProperty(key)) {
            return;
          }
          this[key] = obj[key];
        }, this);
      }, this);
      return this;
    },

    /**
     * Throws a fatal error message.
     *
     * @param {...*} message
     *   The message(s) to display.
     *
     * @throws Error
     *   Throws a new Error object.
     */
    fatal: function (message) {
      throw new Error.apply(Error, arguments);
    },

    /**
     * Retrieves a setting value for this user enhancement.
     *
     * @param {String} name
     *   The machine name of a setting to retrieve.
     * @param {*|Function} [defaultValue]
     *   The default value to use if the setting does not exist. A function can
     *   also be passed and it will be invoked in the even complex logic needs to
     *   occur to determine the value (e.g. parsing the DOM).
     *
     * @return {*}
     *   The setting value, if any.
     */
    getSetting: function (name, defaultValue) {
      if (this.settings[name] === void 0 || this.settings[name] === null) {
        if (this.__parent__) {
          return this.__parent__.getSetting(name, defaultValue);
        }
        return $.isFunction(defaultValue) ? defaultValue.call(this) : defaultValue;
      }
      return this.settings[name];
    },

    /**
     * Invoked upon the initial creation of the user enhancement.
     *
     * This is for non DOM related setup tasks. If you need to traverse DOM
     * elements, use the "ready" method instead.
     *
     * @param {Function} [callback]
     *   Optional. A callback function to invoke when the user enhancement is
     *   initially created.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     *
     * @see UserEnhancement.ready
     */
    init: function (callback) {
      if (!this.initialized) {
        if (callback) {
          this.initCallback = callback;
        }
        else {
          this.initCallback.call(this);
          this.initialized = true;
        }
      }
      return this;
    },

    /**
     * Retrieves a machine name version of a string.
     *
     * @param {String} string
     *   The string to parse.
     *
     * @return {string}
     *   The machine name.
     */
    machineName: function (string) {
      return string.replace(/([A-Z]+[^A-Z]+)/g, '_$1').toLowerCase().replace(/[^a-z0-9-]+/g, '_').replace(/_+/g, '_').replace(/(^_|_$)/g, '');
    },

    /**
     * Namespaces an Event type by appending the name of the user enhancement.
     *
     * @param {String} [type]
     *   The event type.
     *
     * @return {Array}
     *   The namespaced events.
     */
    namespaceEventType: function (type) {
      type = type || '';
      var types = type.split(' ');
      types.forEach(function (type, i) {
        var namespaced = type.split('.');
        namespaced.push(this.name);
        types[i] = namespaced.join('.');
      }, this);
      return types;
    },

    /**
     * Unbinds any events from the UserEnhancement.$element object.
     *
     * @param {String} type
     *   The event type to unbind.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    off: function (type) {
      var namespaced = this.namespaceEventType(type);

      // Remove any attributes from the element.
      if (this.$element[0]) {
        var attributes = this.$element[0].attributes;
        for (var i in attributes) {
          if (!attributes.hasOwnProperty(i)) {
            continue;
          }
          var name = attributes[i].name;

          // Ignore any attributes that don't start with the following.
          if (!/^data-user-enhancement-/.test(name)) {
            continue;
          }

          // Remove either a specific event type namespaced data attribute, if
          // one was specified, or all namespaced types.
          if (new RegExp(type ? namespaced.join('-').replace(/\./g, '-') : this.name.replace(/\./g, '-')).test(name)) {
            attributes.removeNamedItem(name);
          }
        }
      }

      // Remove the DOM handler.
      $(document).off(namespaced.join(' '));
      return this;
    },

    /**
     * Binds and event handler on the UserEnhancement.$element object.
     *
     * @param {String} type
     *   The event type to bind.
     * @param {Function} handler
     *   The event handler callback.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    on: function (type, handler) {
      var namespaced = this.namespaceEventType(type);
      var dataAttribute = 'data-user-enhancement_' + namespaced.join('-').replace(/\./g, '-');
      this.$element.attr(dataAttribute, 'true');
      $(document).on(namespaced.join(' '), '[' + dataAttribute + ']', handler.bind(this));
      return this;
    },

    /**
     * Parses a function's arguments into a proper array.
     *
     * @param {Arguments|Array} args
     *   The function's arguments.
     * @param {Boolean} [bind=true]
     *   Toggle determining whether or not any functions passed should be
     *   properly bound to this user enhancement object. Defaults to true.
     *
     * @return {Array}
     *   The parsed arguments.
     */
    parseArguments: function (args, bind) {
      args = Array.prototype.slice.call(args);
      bind = bind === void 0 || bind ? true : false;

      if (bind) {
        args.forEach(function (arg, i) {
          if ($.isFunction(arg)) {
            args[i] = arg.bind(this);
          }
        }, this);
      }

      return args;
    },

    /**
     * Generates a random string.
     *
     * Taken from http://stackoverflow.com/a/18120932.
     *
     * @param {String} [prefix]
     *   A prefix to use.
     * @param {Number} [length=10]
     *   The length of the string.
     * @param {String} [suffix]
     *   A suffix to use.
     *
     * @return {String}
     *   The randomly generated string.
     */
    random: function (prefix, length, suffix) {
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      prefix = prefix || '';
      suffix = suffix || '';
      return prefix + Array.apply(null, new Array(length || 10)).map(function () {
        return possible[Math.floor(Math.random() * possible.length)];
      }).join('') + suffix;
    },

    /**
     * Prepend a special "DOM ready" attachment.
     *
     * @param {Function} callback
     *   The callback to invoke when the DOM is ready.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    ready: function (callback) {
      var o = {};
      o[this.random('__ready__')] = callback;
      this.attachments = $.extend(o, this.attachments);
      return this;
    },

    /**
     * Scrolls to an element.
     *
     * @param {HTMLElement|jQuery} [element=this.$element]
     *   Optional. The element to scroll to. By default, it will scroll to the
     *   user enhancement's defined $element property.
     * @param {Object} [options={duration:300}]
     *   The options to pass along to the jQuery.animate method. By default,
     *   the duration of the scroll is set to 300, set to 0 for an
     *   instantaneous jump effect.
     * @param {HTMLElement|jQuery} [scrollElement=jQuery('html,body')]
     *   The target element to scroll, by default will scroll the page.
     */
    scrollTo: function (element, options, scrollElement) {
      var $element = element ? $(element) : this.$element;
      if (!$element[0]) {
        this.warning('There is no element to scroll to.');
        return;
      }
      var $scrollElement = scrollElement ? $(scrollElement) : $('html,body');
      options = $.extend({duration: 300}, options);
      var top = $element.offset().top;
      if (options.offset !== void 0) {
        top += options.offset;
        delete options.offset;
      }
      $scrollElement.animate({
        scrollTop: top
      }, options);
    },

    /**
     * Sets a setting for this user enhancement.
     *
     * @param {String|Object} name
     *   The machine name of a setting to set. Optionally, an object can be
     *   passed instead to set multiple key/value settings.
     * @param {*} [value]
     *   The value to set.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    setSetting: function (name, value) {
      // If a specific setting is being set, quickly set it and return.
      if (typeof name === 'string' && value !== void 0) {
        this.settings[name] = this.typeCast(this.getSetting(name), value);
        return this;
      }

      // Iterate over the new settings object.
      var settings = $.isPlainObject(name) ? name : {};
      Object.keys(settings).forEach(function (key) {
        this.settings[key] = this.typeCast(this.getSetting(key), settings[key]);
      }, this);

      return this;
    },

    /**
     * Triggers an event on a defined user enhancement $element.
     *
     * @chainable
     *
     * @return {UserEnhancement}
     *   The user enhancement object.
     */
    trigger: function () {
      this.$element.trigger.apply(this.$element, this.parseArguments(arguments));
      return this;
    },

    /**
     * Truncates a string to a given length.
     *
     * @param {String} string
     *   The string to truncate.
     * @param {Number} length
     *   The length to truncate the string to.
     * @param {Boolean} [useWordBoundary]
     *   Flag indicating whether or not to truncate on a word boundary.
     *
     * @return {String}
     *   The truncated string.
     */
    truncateString: function (string, length, useWordBoundary) {
      var toLong = string.length > length;
      var truncated = toLong ? string.substr(0, length - 1) : string;
      return useWordBoundary && toLong && /\s/.test(truncated) ? truncated.substr(0, truncated.lastIndexOf(' ')) : truncated;
    },

    /**
     * Type casts a new value from an existing value.
     *
     * @param {*} existing
     *   The existing value to test against.
     * @param {*} value
     *   The new value to convert, if needed.
     *
     * @return {*}
     *   The type casted value.
     */
    typeCast: function (existing, value) {
      switch (typeof existing) {
        case 'boolean':
          value = !!value;
          break;
        case 'number':
          value = parseFloat(value);
          break;
        case 'string':
          value += '';
          break;
      }
      return value;
    },

    /**
     * Displays an informative message, if debugging is enabled.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    info: function (message) {
      if (this.debug && window.console && window.console.info) {
        window.console.info.apply(window.console, arguments);
      }
    },

    /**
     * Displays a warning message, if debugging is enabled.
     *
     * @param {...*} message
     *   THe message(s) to display.
     */
    warning: function (message) {
      if (this.debug && window.console && window.console.warn) {
        window.console.warn.apply(window.console, arguments);
      }
    }

  };

})(jQuery, Drupal);
